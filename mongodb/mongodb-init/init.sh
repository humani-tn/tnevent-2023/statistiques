#!/bin/bash

set -e

su mongodb

mongod --bind_ip 0.0.0.0 &
PID=$!

sleep 10
INIT_SCRIPT=$(cat <<EOF
use $MONGO_DATABASE
db.createUser({
    user: '$MONGO_USER',
    pwd: '$MONGO_PASSWORD',
    roles: [
        {
            role: 'readWrite',
            db: 'stats',
        },
    ],
});

EOF
)

(test -e /dbinit.lock && echo "ALREADY INIT") || (echo "LAUNCHING INIT SCRIPT" && echo $INIT_SCRIPT | mongosh && touch /dbinit.lock)


wait $PID
