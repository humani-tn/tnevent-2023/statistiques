import websocket
import _thread
import time
import rel
import json
from datetime import datetime
import pymongo
import os

MONGO_HOST = os.getenv("MONGO_HOST", "localhost")
MONGO_PORT = int(os.getenv("MONGO_PORT", 27017))
MONGO_DATABASE = os.getenv("MONGO_DATABASE", "stats")
WS_URL = os.getenv("WS_URL", "ws://localhost:8080/ws")

client = pymongo.MongoClient(MONGO_HOST, MONGO_PORT)
db = client[MONGO_DATABASE]
db.stats_collection.create_index("timestamp")


def on_message(ws, message):
    json_data = json.loads(message)
    if json_data["msg_type"] == "notification":
        print(json_data["data"])
        data = json_data["data"]
        data["timestamp"] = datetime.now()
        db.stats_collection.insert_one(data)


def on_error(ws, error):
    print(error)


def on_close(ws, close_status_code, close_msg):
    print("### closed ###")


def on_open(ws):
    print("Opened connection")
    ws.send('{"topics": ["/#"]}')


if __name__ == "__main__":
    ws = websocket.WebSocketApp(
        WS_URL,
        on_open=on_open,
        on_message=on_message,
        on_error=on_error,
        on_close=on_close,
    )

    ws.run_forever(
        dispatcher=rel, reconnect=5
    )  # Set dispatcher to automatic reconnection, 5 second reconnect delay if connection closed unexpectedly
    rel.signal(2, rel.abort)  # Keyboard Interrupt
    rel.dispatch()
